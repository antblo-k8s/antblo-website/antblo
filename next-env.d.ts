/// <reference types="next" />
/// <reference types="next/types/global" />

declare module "*.glsl" {
  const content: string;
  export default content;
}
declare module "*.vert" {
  const content: string;
  export default content;
}
declare module "*.frag" {
  const content: string;
  export default content;
}
declare module "*.mp3" {
  const content: string;
  export default content;
}
declare module "*.fft" {
  const content: Uint8Array;
  export default content;
}
declare module "*.svg" {
  const content: any;
  export default content;
}
