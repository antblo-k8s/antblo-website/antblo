import React, { Component } from "react";
import { CV_SV } from "../../src/constants/CV_LINKS";

class CvSvRedirectPage extends Component {
  constructor(props: Readonly<{}>) {
    super(props);
  }

  async componentDidMount() {
    if (typeof window !== undefined) window.location.href = CV_SV;
  }

  render() {
    return <></>;
  }
}

export default CvSvRedirectPage;
