import React, { Component } from "react"
import { CV_EN } from "../../src/constants/CV_LINKS"
class CvEnRedirectPage extends Component {
  constructor(props: Readonly<{}>) {
    super(props)
  }

  async componentDidMount() {
    if (typeof window !== undefined) window.location.href = CV_EN
  }

  render() {
    return <></>
  }
}
export default CvEnRedirectPage
