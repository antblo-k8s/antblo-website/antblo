FROM node:14.14.0-buster AS base

FROM base AS debug
WORKDIR /app
RUN apt update -y && apt install curl iptables sudo sshfs -y
RUN sudo curl -fL https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence
RUN sudo chmod a+x /usr/local/bin/telepresence

FROM base AS build
ADD . /build
WORKDIR /build
RUN npm i
RUN npm run build

FROM node:14.14.0-buster-slim AS final
COPY --from=BUILD /build/out /antblo
RUN npm i serve -g
EXPOSE 5000
CMD serve /antblo
