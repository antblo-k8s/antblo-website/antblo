precision lowp float;

uniform vec2 inputPixel;
uniform float time;
uniform float song;
uniform float rain_dist;

const float PHI = 1.61803398874989484820459;

float rand1D(in float x) {
  return fract(tan(abs(x * PHI - x) * 512.724));
}

float rand2D(in vec2 xy) {
  return fract(tan(distance(xy * PHI, xy) * 512.724) * xy.x);
}

float rand3D(in vec3 xy) {
  return fract(tan(distance(xy * PHI, xy) * 512.724) * xy.x);
}

void main()
{
  vec2 uv = gl_FragCoord.xy / inputPixel.y;
  vec3 col = vec3(0.0);
  float droplet_and_splash = 0.;
  float horizon_height = 0.2;

  const int num_layers = 5;
  for(int i = 0; i < num_layers; i++){
    float i_f = float(i);
    float num_layers_f = float(num_layers);
    float scaled_layer_index = (i_f+1.)/num_layers_f;

    uv.x += rand1D(scaled_layer_index);
    
    float dist = scaled_layer_index*(horizon_height);
    vec2 aspect = scaled_layer_index*vec2(20., 20.);
    dist-=0.02;

    float ripple_line = uv.y - dist;

    vec2 box = uv;
    float box_time = (1.05-scaled_layer_index)*time+rand1D(floor(box.x*aspect.x)-PHI);

    box.y += box_time - dist;
    box *= aspect;

    vec2 box_id = floor(box);
    box = fract(box);

    float droplet_pos_y = 0.25 + 0.5*rand1D(box_id.x+box_id.y+i_f);
    float droplet = 0.2*smoothstep(0.01, 0.0, abs(box.x - droplet_pos_y))*smoothstep(0.5, 0.0, box.y)*smoothstep(0.0, 0.05, box.y) * step(0.0, ripple_line);

    vec2 stripe_aspect = vec2(aspect.x, 1.0);
    vec2 stripe = fract(uv*stripe_aspect);
    float splash_compression = 1.0/horizon_height;
    float splash_time = fract((box_time)*aspect.y);
    float splash_rim = 0.25*splash_time;
    vec2 splash_pos = (stripe-vec2(droplet_pos_y,dist ))*vec2(1.0, aspect.y*splash_compression);
    float splash = (1. - splash_time)*smoothstep(splash_rim, 0.0, abs(length(splash_pos)));
    splash *= smoothstep(0.1*splash_time, splash_rim, abs(length(splash_pos)));

    //droplet_and_splash+=box_id.x/100.;
    droplet_and_splash+=droplet;
    droplet_and_splash+=splash;
    //if (box.x<.02)droplet_and_splash += 0.2;
    //if (box.y<.02)droplet_and_splash += 0.2;
  }
  //droplet_and_splash+=splash_pos.x*splash_pos.y;
  //droplet_and_splash+=max(0., (1. + ripple.y)*(box_time*aspect.y - box_id.y));

  //droplet_and_splash+= 0.2*step(abs(ripple_line), 0.001); // Ripple line.

  //droplet_and_splash*=step(dist, uv.y); // Mask droplet at end.

  col += vec3(droplet_and_splash);
  gl_FragColor = vec4(col, 0.0);
}
